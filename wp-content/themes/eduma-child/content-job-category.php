<?php
$theme_options_data = get_theme_mods();

$classes   = array();
$classes[] = 'col-sm-12';
?>
<article id="post-<?php the_ID(); ?>" <?php post_class( $classes ); ?>>
	<div class="content-inner">
		<?php
		do_action( 'thim_entry_top', 'full' ); ?>
		<div class="entry-content">
		
			
				<header class="entry-header">
					<?php
					if ( !isset( $theme_options_data['thim_show_date'] ) || $theme_options_data['thim_show_date'] == 1 ) {
						?>
						<div class="date-meta"><?php echo get_the_date( "d\<\i\>\ F\<\/\i\>\ " ) ?></div>
					<?php
					}
					?>
					
					<div class="entry-contain">
						<?php the_title( sprintf( '<h2 class="entry-title"><a href="%s" rel="bookmark">', esc_url( get_permalink() ) ), '</a></h2>' ); ?>
						
					</div>
				</header>
				<!-- .entry-header -->
				<div class="entry-summary">
					<?php
					the_excerpt();
					?>
				</div><!-- .entry-summary -->
				<div class="readmore">
					<a href="<?php echo esc_url( get_permalink() ); ?>"><?php echo esc_html__('Read More','eduma'); ?></a>
				</div>
			
		</div>
	</div>
</article><!-- #post-## -->