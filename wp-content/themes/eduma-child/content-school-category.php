<?php
$theme_options_data = get_theme_mods();

$classes   = array();
$classes[] = 'col-sm-12';
?>
<article id="post-<?php the_ID(); ?>" <?php post_class( $classes ); ?>>
	<div class="content-inner">
	
		
		<div class="entry-content">
				<div class="col-md-5">
					<div style="background-image:url('<?php echo get_the_post_thumbnail_url(); ?>');" class="archieve-custom-bg-featued"></div>
				</div>
			
				<header class="col-md-7 entry-header">
					
					
					
						<?php the_title( sprintf( '<h2 class="entry-title"><a href="%s" rel="bookmark">', esc_url( get_permalink() ) ), '</a></h2>' ); ?>
						<ul class="entry-meta">
							<li class="author">
								<span>Establised on</span>
								<?php echo get_post_meta(get_the_ID(),'wpcf-established-year',true); ?>			
							</li>
							<li class="author">
								<span>Locations</span>
								<?php $schools_area_city = wp_get_post_terms($post->ID, 'schools-area-city', array("fields" => "all"));
					
						$schools_area_state = wp_get_post_terms($post->ID, 'schools-area-state', array("fields" => "all"));
//echo '<a href="'.get_term_link($schools_area_city[0]).'">'.$schools_area_city[0]->name.'</a>';
echo $schools_area_city[0]->name;
echo " ";
//echo '<a href="'.get_term_link($schools_area_state[0]).'">'.$schools_area_state[0]->name.'</a>';
echo $schools_area_state[0]->name;
?>	
							</li>

							<li class="author">
								<span>Rating</span>
								<?php echo do_shortcode('[RICH_REVIEWS_SNIPPET category="page" stars_only="true"]'); ?>		
							</li>
								
						</ul>

						<div class="facilities-archieve">
								<div class="title-fac-archieve">Facilities </div>
								<div class="facilities-row">
								<?php 
					
									$facilities_arr = get_post_meta(get_the_ID(),'wpcf-facilities',true);
									
									foreach ($facilities_arr as $facility) {
									echo $facility[0];
									if ($facility != end($facilities_arr))
									{
										echo ', ';
									} }
									
									?> </div>

						</div>
					
				
				
				<div class="readmore">
					<a href="<?php echo esc_url( get_permalink() ); ?>"><?php echo esc_html__('Read More','eduma'); ?></a>
				</div>
				</header>
			
		</div>
	</div>
</article><!-- #post-## -->