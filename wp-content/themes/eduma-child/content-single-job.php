<?php
/**
 * @package thim
 */

$theme_options_data = get_theme_mods();
?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>


	<div class="page-content-inner">
		<header class="entry-header">
		

			<?php the_title( '<h1 class="entry-title">', '</h1>' ); ?>
		<?php
				$qb_category = wp_get_post_terms($post->ID, 'job-category', array("fields" => "all"));
		?>
			<ul class="entry-meta">


			
				

				<li class="author">
					<span>Category</span>
					<span class="value"><?php echo $qb_category[0]->name; ?></span>		

				</li>

				<li class="author">
					<span>Posted at</span>
					<span class="value"><?php echo the_date(); ?></span>		

				</li>

				

								
			</ul>


		</header>
		<?php
		/* Video, Audio, Image, Gallery, Default will get thumb */
		do_action( 'thim_entry_top', 'full' );
		?>
		<!-- .entry-header -->
		<div class="entry-content learnpress-content learn-press">
			





			<div class="course-summary">

			

				<div id="course-learning">

					<?php echo the_content(); ?>

								<?php if (get_post_meta(get_the_ID(),'wpcf-question-bank-file',true)) 
								{
									?>


								<div class="qb-download-wrapper col-md-10">
										
										<div class="col-md-8">
												<span class="qb-tags-item"><div class="qb-tag-title">Reference file</div>

												 </span>
												<a href="<?php echo get_post_meta(get_the_ID(),'wpcf-question-bank-file',true); ?>"><button>Download Now</button> </a>

										</div>
										<div class="col-md-4">
												<i class="fa fa-cloud-download" aria-hidden="true"></i>
										</div>
								</div>	

							<?php } ?>


					</div>

					

					

				</div>


			<?php
			wp_link_pages( array(
				'before' => '<div class="page-links">' . esc_html__( 'Pages:', 'eduma' ),
				'after'  => '</div>',
			) );
			?>
		</div>
		<div class="entry-tag-share">
			<div class="row">
				<div class="col-sm-6">
					<?php
					if ( get_the_tag_list() ) {
						echo get_the_tag_list( '<p class="post-tag"><span>' . esc_html__( "Tag:", 'eduma' ) . '</span>', ', ', '</p>' );
					}
					?>
				</div>
				<div class="col-sm-6">
					<?php do_action( 'thim_social_share' ); ?>
				</div>
			</div>
		</div>
	

		<?php
		$prev_post = get_previous_post();
		$next_post = get_next_post();
		?>
		<?php if ( !empty( $prev_post ) || !empty( $next_post ) ): ?>
			<div class="entry-navigation-post">
				<?php
				if ( !empty( $prev_post ) ):
					?>
					<div class="prev-post">
						<p class="heading"><?php echo esc_html__( 'Previous post', 'eduma' ); ?></p>
						<h5 class="title">
							<a href="<?php echo get_permalink( $prev_post->ID ); ?>"><?php echo esc_html( $prev_post->post_title ); ?></a>
						</h5>

						<div class="date">
							<?php echo get_the_date( get_option( 'date_format' ) ); ?>
						</div>
					</div>
				<?php endif; ?>

				<?php
				if ( !empty( $next_post ) ):
					?>
					<div class="next-post">
						<p class="heading"><?php echo esc_html__( 'Next post', 'eduma' ); ?></p>
						<h5 class="title">
							<a href="<?php echo get_permalink( $next_post->ID ); ?>"><?php echo esc_html( $next_post->post_title ); ?></a>
						</h5>

						<div class="date">
							<?php echo get_the_date( 'j F, Y', $next_post->ID ); ?>
						</div>
					</div>
				<?php endif; ?>
			</div>

		<?php endif; ?>

		<?php
		get_template_part( 'inc/related' );
		?>
	</div>
</article>