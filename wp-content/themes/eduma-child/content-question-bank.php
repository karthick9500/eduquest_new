<?php
$theme_options_data = get_theme_mods();

$classes   = array();
$classes[] = 'col-sm-12';
?>
<article id="post-<?php the_ID(); ?>" <?php post_class( $classes ); ?>>
	<div class="content-inner">
	
		<div class="row col-md-12 item-event tp_event type-tp_event all-question-bank">
				
					<div class="col-md-2 image-svg">
						<i class="fa fa-file-pdf-o" aria-hidden="true"></i>
					</div>
					<div class="col-md-10 event-wrapper">
						
							<?php the_title( sprintf( '<h2 class="entry-title"><a href="%s" rel="bookmark">', esc_url( get_permalink() ) ), '</a></h2>' ); ?>
						
						
					</div>
				</div>


	</div>
</article><!-- #post-## -->