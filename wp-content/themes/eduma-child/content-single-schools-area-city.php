<?php
/**
 * @package thim
 */

$theme_options_data = get_theme_mods();
?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>


	<div class="page-content-inner">
		<header class="entry-header">
		

			<?php the_title( '<h1 class="entry-title">', '</h1>' ); ?>
			<ul class="entry-meta">

				<?php
				$school_category = wp_get_post_terms($post->ID, 'school-category', array("fields" => "all"));
		?>
			
				<li class="author">
					<span>Established on</span>
					<span class="value"><?php echo get_post_meta(get_the_ID(),'wpcf-established-year',true); ?></span>		

				</li>

				<li class="author">
					<span>Category</span>
					<span class="value"><?php echo $school_category[0]->name; ?></span>		

				</li>

				<li class="entry-date">
					<span>Location</span>
					<span class="value">

						<?php $schools_area_city = wp_get_post_terms($post->ID, 'schools-area-city', array("fields" => "all"));
					
						$schools_area_state = wp_get_post_terms($post->ID, 'schools-area-state', array("fields" => "all"));
echo '<a href="'.get_term_link($schools_area_city[0]).'">'.$schools_area_city[0]->name.'</a>';
echo " ";
echo '<a href="'.get_term_link($schools_area_state[0]).'">'.$schools_area_state[0]->name.'</a>';
?>


					</span>
				</li>

				<li class="entry-date">
					<span>Rating</span>
					<span class="value"><?php echo do_shortcode('[RICH_REVIEWS_SNIPPET category="page" stars_only="true"]'); ?></span>
				</li>
								
			</ul>
		</header>
		<?php
		/* Video, Audio, Image, Gallery, Default will get thumb */
		do_action( 'thim_entry_top', 'full' );
		?>
		<!-- .entry-header -->
		<div class="entry-content learnpress-content learn-press">
			





			<div class="course-summary">


		<div class="intro-basic">

								<div class="row intro-basic-tab">

									<span class="col-md-4">
									
										<div class="contact-head">Established on</div>
									</span>
									<div class="col-md-8 contact-answ"><?php echo get_post_meta(get_the_ID(),'wpcf-established-year',true); ?></div>

								</div>

								<div class="row intro-basic-tab">

									<span class="col-md-4">
										
										<div class="contact-head">Management</div>
									</span>
									<div class="col-md-8 contact-answ"> <?php echo get_post_meta(get_the_ID(),'wpcf-management',true); ?></div>

								</div>

							

								<div class="row intro-basic-tab">

									<span class="col-md-4">
										
										<div class="contact-head">Coaching type</div>
									</span>
									<div class="col-md-8 contact-answ"> <?php echo get_post_meta(get_the_ID(),'wpcf-coaching-type',true); ?></div>

								</div>
			
		</div>



	
		


<div id="course-learning">

	<div class="course-tabs">

		<ul class="nav nav-tabs">
			<li role="presentation" class="active">
				<a href="#tab-course-description" data-toggle="tab">
					<i class="fa fa-bookmark"></i>
					<span>Description</span>
				</a>
			</li>
			<li class="">
				<a href="#tab-course-curriculum" data-toggle="tab">
					<i class="fa fa-cube"></i>
					<span>Contact details</span>
				</a>
			</li>
			<li role="presentation" class="">
				<a href="#tab-course-instructor" data-toggle="tab">
					<i class="fa fa-user"></i>
					<span>Image Gallery</span>
				</a>
			</li>
							<li role="presentation" class="">
					<a href="#tab-course-review" data-toggle="tab">
						<i class="fa fa-comments"></i>
						<span>Reviews</span>
						
					</a>
				</li>
					</ul>

		<div class="tab-content">
			<div class="tab-pane active" id="tab-course-description">
											<div class="thim-course-content">
													<div>
													<h4 class="fa-1x clr-DarkSlateBlue padtb10">About Institute</h4>
													<?php echo get_post_meta(get_the_ID(),'wpcf-about-institute',true); ?>
													</div>


													<div>
													<h4 class="fa-1x clr-DarkSlateBlue padtb10">Vision</h4>
													<p class="text-justify"><?php echo get_post_meta(get_the_ID(),'wpcf-vision',true); ?></p>
													</div>





													<div>
													<h4 class="fa-1x clr-DarkSlateBlue padtb10">Mission</h4>
													<p class="text-justify"><?php echo get_post_meta(get_the_ID(),'wpcf-mission',true); ?></p>
													</div>
											</div>
						<div class="thim-course-info">
			<h3 class="title">Facilities</h3>
			<ul>



					<?php 
					
					$facilities_arr = get_post_meta(get_the_ID(),'wpcf-facilities',true);
					
					foreach ($facilities_arr as $facility) {
					?>

					<li class="lectures-feature">
						<i class="fa fa-files-o"></i>
						<span class="label"><?php echo $facility[0] ?></span>
						
					</li>


					<?php
					
					}
					
					?>
				
			
			</ul>
				</div>
										<ul class="thim-social-share"><li class="heading">Share:</li><li><div class="facebook-social"><a target="_blank" class="facebook" href="https://www.facebook.com/sharer.php?u=http%3A%2F%2Fdev.eduquesthub.com%2Fcourses%2Flearnpress-101%2F" title="Facebook"><i class="fa fa-facebook"></i></a></div></li><li><div class="googleplus-social"><a target="_blank" class="googleplus" href="https://plus.google.com/share?url=http%3A%2F%2Fdev.eduquesthub.com%2Fcourses%2Flearnpress-101%2F&amp;title=Introduction%20LearnPress%20%26%238211%3B%20LMS%20plugin" title="Google Plus" onclick="javascript:window.open(this.href, &quot;&quot;, &quot;menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=600,width=600&quot;);return false;"><i class="fa fa-google"></i></a></div></li><li><div class="twitter-social"><a target="_blank" class="twitter" href="https://twitter.com/share?url=http%3A%2F%2Fdev.eduquesthub.com%2Fcourses%2Flearnpress-101%2F&amp;text=Introduction%20LearnPress%20%26%238211%3B%20LMS%20plugin" title="Twitter"><i class="fa fa-twitter"></i></a></div></li></ul>			</div>
			<div class="tab-pane" id="tab-course-curriculum">
								<div class="course-curriculum" id="learn-press-course-curriculum">
	<div class="thim-curriculum-buttons">
		
	
	</div>	

	
								

								<div class="row   contact-item-tab">

									<span class="col-md-4">
										<i class="fa fa-user" aria-hidden="true"></i> 
										<div class="contact-head">Contact person</div>
									</span>
									<div class="col-md-8 contact-answ"><?php echo get_post_meta(get_the_ID(),'wpcf-contact-person',true); ?></div>

								</div>

								<div class="row  contact-item-tab">

									<span class="col-md-4">
										<i class="fa fa-map-marker" aria-hidden="true"></i> 
										<div class="contact-head">Address</div>
									</span>
									<div class="col-md-8 contact-answ"> <?php echo get_post_meta(get_the_ID(),'wpcf-address',true); ?></div>

								</div>

								<div class="row  contact-item-tab">

									<span class="col-md-4">
										<i class="fa fa-address-card-o" aria-hidden="true"></i> 
										<div class="contact-head">Pincode</div>
									</span>
									<div class="col-md-8 contact-answ"><?php echo get_post_meta(get_the_ID(),'wpcf-pin-code',true); ?></div>

								</div>

								<div class="row contact-item-tab">

									<span class="col-md-4">
										<i class="fa fa-globe" aria-hidden="true"></i> 
										<div class="contact-head">Website</div>
									</span>
									<div class="col-md-8 contact-answ"><?php echo get_post_meta(get_the_ID(),'wpcf-website',true); ?></div>

								</div>

								<div class="row contact-item-tab">

									<span class="col-md-4">
										<i class="fa fa-phone" aria-hidden="true"></i> 
										<div class="contact-head">Phone</div>
									</span>
									<div class="col-md-8 contact-answ"><?php echo get_post_meta(get_the_ID(),'wpcf-contact-numbers',true); ?></div>

								</div>

								<div class="row  contact-item-tab">

									<span class="col-md-4">
										<i class="fa fa-tag" aria-hidden="true"></i> 
										<div class="contact-head">Area</div>
									</span>
									<div class="col-md-8 contact-answ">

											<?php 
											$terms = get_terms( array(
								    		'taxonomy' => 'schools-area-city',
								    
											) ); 
											foreach ($terms as $term) { ?>
										<a href="<?php echo get_term_link($term)?>"class="area-tag"><?php echo $term->name; ?></a>
									<?php	} ?>
									<?php 
											$terms = get_terms( array(
								    		'taxonomy' => 'schools-area-state',
								    
											) ); 
											foreach ($terms as $term) { ?>
										<a href="<?php echo get_term_link($term)?>"class="area-tag"><?php echo $term->name; ?></a>
									<?php	} ?>
									</div>

								</div>


								



								
			
	
</div>			</div>
			

			<div class="tab-pane" id="tab-course-instructor">
						
							<?php echo do_shortcode('[inpost_pixedelic_camera slide_width="800" slide_height="600" thumb_width="75" thumb_height="75" post_id="'.get_the_ID().'" skin="camera_amber_skin" alignment="" time="7000" transition_period="1500" bar_direction="leftToRight" data_alignment="topLeft" easing="swing" slide_effects="random" grid_difference="250" thumbnails="1" pagination="0" auto_advance="1" hover="1" play_pause_buttons="1" pause_on_click="1" id="" random="0" group="0" show_in_popup="0" album_cover="" album_cover_width="200" album_cover_height="200" popup_width="800" popup_max_height="600" popup_title="Gallery" type="pixedelic_camera"][/inpost_pixedelic_camera]'); ?>
			</div>




				<div class="tab-pane" id="tab-course-review">
					<div class="course-rating">
						<h3>Reviews</h3>


						<?php echo do_shortcode('[RICH_REVIEWS_SNIPPET category="page" ]'); ?>


						<?php echo do_shortcode('[RICH_REVIEWS_SHOW num="6" category="page" ]'); ?>
						


					</div>


			
				</div>



					</div>


				<div class="review-post-form-wrapper">
					<h3 class="review-post-form-title">Post a review</h3>
					<?php echo do_shortcode('[RICH_REVIEWS_FORM category="page"]'); ?>
				</div>

	</div>

</div>


	
</div>


			<?php
			wp_link_pages( array(
				'before' => '<div class="page-links">' . esc_html__( 'Pages:', 'eduma' ),
				'after'  => '</div>',
			) );
			?>
		</div>
		<div class="entry-tag-share">
			<div class="row">
				<div class="col-sm-6">
					<?php
					if ( get_the_tag_list() ) {
						echo get_the_tag_list( '<p class="post-tag"><span>' . esc_html__( "Tag:", 'eduma' ) . '</span>', ', ', '</p>' );
					}
					?>
				</div>
				<div class="col-sm-6">
					<?php do_action( 'thim_social_share' ); ?>
				</div>
			</div>
		</div>
		

		<?php
		$prev_post = get_previous_post();
		$next_post = get_next_post();
		?>
		<?php if ( !empty( $prev_post ) || !empty( $next_post ) ): ?>
			<div class="entry-navigation-post">
				<?php
				if ( !empty( $prev_post ) ):
					?>
					<div class="prev-post">
						<p class="heading"><?php echo esc_html__( 'Previous School', 'eduma' ); ?></p>
						<h5 class="title">
							<a href="<?php echo get_permalink( $prev_post->ID ); ?>"><?php echo esc_html( $prev_post->post_title ); ?></a>
						</h5>

						
					</div>
				<?php endif; ?>

				<?php
				if ( !empty( $next_post ) ):
					?>
					<div class="next-post">
						<p class="heading"><?php echo esc_html__( 'Next School', 'eduma' ); ?></p>
						<h5 class="title">
							<a href="<?php echo get_permalink( $next_post->ID ); ?>"><?php echo esc_html( $next_post->post_title ); ?></a>
						</h5>

						
					</div>
				<?php endif; ?>
			</div>

		<?php endif; ?>

		<?php
		get_template_part( 'inc/related' );
		?>
	</div>
</article>