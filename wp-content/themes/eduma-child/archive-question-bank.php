<?php
if ( have_posts() ) :?>
	<div class="row blog-content archive_switch blog-list">




	
<div class="list-tab-event">
	<ul class="nav nav-tabs">
		<li class="active">
			<a href="#tab-all-questions" data-toggle="tab">ALL</a>
		</li>
		<li class="">
			<a href="#tab-upcoming" data-toggle="tab">Quesiton bank by Subject</a>
		</li>
		<li >
			<a href="#tab-expired" data-toggle="tab">Question bank by Exam</a>
		</li>
	</ul>
	<div class="tab-content thim-list-event">
		<div role="tabpanel" class="tab-pane fade active in" id="tab-all-questions">

					<?php
					/* Start the Loop */
					while ( have_posts() ) : the_post();
						get_template_part( 'content-question-bank' );
					endwhile;
					?>


			
			</div>
			<div role="tabpanel" class="tab-pane fade" id="tab-upcoming">


									<?php 
											$terms = get_terms( array(
								    		'taxonomy' => 'subject-type',
								    		'hide_empty' => false,
								    		
								    
											) ); 
											foreach ($terms as $term) { ?>
									

										<div class="item-event post-2953 tp_event type-tp_event status-tp-event-upcoming has-post-thumbnail hentry pmpro-has-access">
											
											<div class="image" style="float:left;">
												<img src="http://educationwp.thimpress.com/wp-content/uploads/2015/11/event-4-450x233.jpg" alt="event-4" title="event-4" width="450" height="233">
											</div>
											<div class="event-wrapper">
												<h5 class="title">
													<a href="<?php echo get_term_link($term); ?>" ><?php echo $term->name; ?></a>
												</h5>
												
												<div class="description">
													<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard.</p>
												</div>
											</div>

											<div class="time-from">
												<div class="date"> <?php echo $term->count; ?></div>
												<div class="month"> Items</div>
											</div>


											</div>



									<?php	} ?>

								

				
				</div>
				<div role="tabpanel" class="tab-pane fade" id="tab-expired">
					
						
					<?php 
											$terms_exam = get_terms( array(
								    		'taxonomy' => 'exam-type',
								    		'hide_empty' => false,
								    		
								    
											) ); 
											foreach ($terms_exam as $term_exam) { ?>
									

										<div class="item-event post-2953 tp_event type-tp_event status-tp-event-upcoming has-post-thumbnail hentry pmpro-has-access">
											
											<div class="image" style="float:left;">
												<img src="http://educationwp.thimpress.com/wp-content/uploads/2015/11/event-4-450x233.jpg" alt="event-4" title="event-4" width="450" height="233">
											</div>
											<div class="event-wrapper">
												<h5 class="title">
													<a href="<?php echo get_term_link($term_exam); ?>" ><?php echo $term_exam->name; ?></a>
												</h5>
												
												<div class="description">
													<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard.</p>
												</div>
											</div>

											<div class="time-from">
												<div class="date"> <?php echo $term_exam->count; ?></div>
												<div class="month"> Items</div>
											</div>


											</div>



									<?php	} ?>		
							
									
				</div>
			</div>
		</div>



	
		
	</div>
	<?php
	thim_paging_nav();
else :
	get_template_part( 'content-question-bank', 'none' );
endif;