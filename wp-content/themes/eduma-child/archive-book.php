<?php
if ( have_posts() ) :?>





<div class="thim-course-grid">


   <div class="row blog-content archive_switch blog-list">
      <?php
      /* Start the Loop */
      while ( have_posts() ) : the_post();
        ?>


   <article class="course-grid-4 lpr_course">
      <div class="course-item">

      	
         <div class="course-thumbnail"> <a href="<?php echo esc_url( get_permalink() ); ?>" ><div class="icon-thumb-cards" style="background-image:url(<?php the_post_thumbnail_url( 'medium' ); ?>);" ></div> </a> <a class="course-readmore" href="<?php echo esc_url( get_permalink() ); ?>">Read More</a></div>
         <div class="thim-course-content">
            
            <?php the_title( sprintf( '<h2 class="course-title"><a href="%s" rel="bookmark">', esc_url( get_permalink() ) ), '</a></h2>' ); ?>
            <div class="course-meta">
               <div class="course-students">
                   <a class="course-readmore" href="<?php echo esc_url( get_permalink() ); ?>">Read More</a>
               </div>
               <!-- <div class="course-comments-count">
                  <div class="value"><i class="fa fa-comment"></i>3</div>
               </div>  -->

               <div class="course-price" itemprop="offers" itemscope="" itemtype="http://schema.org/Offer">
                  <div class="value free-course" itemprop="price" content="Free"><?php echo do_shortcode('[RICH_REVIEWS_SNIPPET category="page" stars_only="true"]'); ?>  </div>
                  <meta itemprop="priceCurrency" content="$">
               </div>
            </div>
         </div>
      </div>
   </article>

   <?php 
      endwhile;
      ?>
   </div>

</div>


	<?php
	thim_paging_nav();
else :
	get_template_part( 'content-question-bank', 'none' );
endif;