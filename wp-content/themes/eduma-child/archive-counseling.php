<?php
if ( have_posts() ) :?>





<div class="thim-course-grid">

	<?php 
	$terms = get_terms( array(
	'taxonomy' => 'counseling-category',
	'hide_empty' => false,


	) ); 
	foreach ($terms as $term) { ?>

   <article class="course-grid-3 lpr_course">
      <div class="course-item">
         <div class="course-thumbnail"> <a href="<?php echo get_term_link($term); ?>" ><div class="icon-thumb-cards" style="background-image:url(<?php echo get_term_meta($term->term_id,'wpcf-category-image',true); ?>);" ></div> </a> <a class="course-readmore" href="<?php echo get_term_link($term); ?>">Read More</a></div>
         <div class="thim-course-content">
            
            <h2 class="course-title"> <a rel="bookmark" href="<?php echo get_term_link($term); ?>" ><?php echo $term->name; ?></a></h2>
            <div class="course-meta">
               <div class="course-students">
                   <a class="course-readmore" href="<?php echo get_term_link($term); ?>">Read More</a>
               </div>
               <!-- <div class="course-comments-count">
                  <div class="value"><i class="fa fa-comment"></i>3</div>
               </div>  -->

               <div class="course-price" itemprop="offers" itemscope="" itemtype="http://schema.org/Offer">
                  <div class="value free-course" itemprop="price" content="Free"> <?php echo $term->count; ?> Colleges</div>
                  <meta itemprop="priceCurrency" content="$">
               </div>
            </div>
         </div>
      </div>
   </article>

   <?php	} ?>

</div>



	<?php
	thim_paging_nav();
else :
	get_template_part( 'content-question-bank', 'none' );
endif;