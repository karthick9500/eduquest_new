<?php
$theme_options_data = get_theme_mods();

$classes   = array();
$classes[] = 'col-sm-12';
?>
<article id="post-<?php the_ID(); ?>" <?php post_class( $classes ); ?>>
	<div class="content-inner">
	
		
		<div class="entry-content">
				<div class="col-md-5">
					<div style="background-image:url('<?php echo get_the_post_thumbnail_url(); ?>');" class="archieve-custom-bg-featued"></div>
				</div>
			
				<header class="col-md-7 entry-header">
					
					
					
						<?php the_title( sprintf( '<h2 class="entry-title"><a href="%s" rel="bookmark">', esc_url( get_permalink() ) ), '</a></h2>' ); ?>
						<ul class="entry-meta">
							<li class="author">
								<span>Establised on</span>
								<?php echo get_post_meta(get_the_ID(),'wpcf-established-year',true); ?>			
							</li>
							<li class="author">
								<span>Location</span>
								<?php 
											$terms = get_terms( array(
								    		'taxonomy' => 'college-area-city',
								    		
								    
											) ); 
											foreach ($terms as $term) { ?>
										<a href="<?php echo get_term_link($term)?>" class="area-tag-archieve"><?php echo $term->name; ?></a>, 
									<?php	} ?>

									<?php 
											$terms = get_terms( array(
								    		'taxonomy' => 'college-area-state',
								    		
								    
											) ); 
											foreach ($terms as $term) { ?>
										<a href="<?php echo get_term_link($term)?>" class="area-tag-archieve"><?php echo $term->name; ?></a>
									<?php	} ?>		
							</li>

							<li class="author">
								<span>Rating</span>
								4/5			
							</li>
								
						</ul>


						<div class="facilities-archieve">
								<div class="title-fac-archieve">Facilities </div>
								<div class="facilities-row">
								<?php 
					
									$facilities_arr = get_post_meta(get_the_ID(),'wpcf-facilities',true);
									
									
									foreach ($facilities_arr as $facility) {
									echo $facility[0];
									if ($facility != end($facilities_arr))
									{
										echo ', ';
									} }
									
									?> </div>

						</div>

						
					
				
				
				<div class="readmore">
					<a href="<?php echo esc_url( get_permalink() ); ?>"><?php echo esc_html__('Read More','eduma'); ?></a>
				</div>
				</header>
			
		</div>
	</div>
</article><!-- #post-## -->