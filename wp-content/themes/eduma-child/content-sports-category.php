<?php
$theme_options_data = get_theme_mods();

$classes   = array();
$classes[] = 'col-sm-12';
?>
<article id="post-<?php the_ID(); ?>" <?php post_class( $classes ); ?>>
	<div class="content-inner">
	
		
		<div class="entry-content">
				<div class="col-md-5">
					<div style="background-image:url('<?php echo get_the_post_thumbnail_url(); ?>');" class="archieve-custom-bg-featued"></div>
				</div>
			
				<header class="col-md-7 entry-header">
					
					
					
						<?php the_title( sprintf( '<h2 class="entry-title"><a href="%s" rel="bookmark">', esc_url( get_permalink() ) ), '</a></h2>' ); ?>
					


						<div class="facilities-archieve">
								
								<div class="facilities-row">
								
								<?php 

								$about_text =  get_post_meta(get_the_ID(),'wpcf-overview',true);;
								$about_newtext = strlen($about_text) > 50 ? substr($about_text,0,550)."..." : $about_text;

								echo $about_newtext;
								 ?>

								</div>

						</div>

						
					
				
				
				<div class="readmore">
					<a href="<?php echo esc_url( get_permalink() ); ?>"><?php echo esc_html__('Read More','eduma'); ?></a>
				</div>
				</header>
			
		</div>
	</div>
</article><!-- #post-## -->