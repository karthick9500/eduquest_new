<?php
/**
 * @package thim
 */

$theme_options_data = get_theme_mods();
?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>


	<div class="page-content-inner">
		<header class="entry-header">
			<?php the_title( '<h1 class="entry-title">', '</h1>' ); ?>
			<?php thim_entry_meta(); ?>
		</header>
		<div class="row">
				<div class="col-md-4">
				<?php
				/* Video, Audio, Image, Gallery, Default will get thumb */
				do_action( 'thim_entry_top', 'full' );
				?>
				</div>
				<!-- .entry-header -->
				<div class="col-md-8 entry-content">
					
									<div class="intro-basic">

											<div class="row intro-basic-tab">

												<span class="col-md-4">
												
													<div class="contact-head">Author</div>
												</span>
												<div class="col-md-8 contact-answ"><?php echo get_post_meta(get_the_ID(),'wpcf-author',true); ?></div>

											</div>

											<div class="row intro-basic-tab">

												<span class="col-md-4">
													
													<div class="contact-head">Publisher</div>
												</span>
												<div class="col-md-8 contact-answ"> <?php echo get_post_meta(get_the_ID(),'wpcf-publisher',true); ?></div>

											</div>

											<div class="row intro-basic-tab">

												<span class="col-md-4">
													 
													<div class="contact-head">Publisher Address</div>
												</span>
												<div class="col-md-8 contact-answ">
												
														<?php echo get_post_meta(get_the_ID(),'wpcf-publisher-address',true); ?>
												</div>

											</div>

											<div class="row intro-basic-tab">

												<span class="col-md-4">
													
													<div class="contact-head">Publisher email</div>
												</span>
												<div class="col-md-8 contact-answ"> <?php echo get_post_meta(get_the_ID(),'wpcf-publisher-email',true); ?></div>

											</div>


											<div class="row intro-basic-tab">

												<span class="col-md-4">
													
													<div class="contact-head">City/Country</div>
												</span>
												<div class="col-md-8 contact-answ"> <?php echo get_post_meta(get_the_ID(),'wpcf-citycountry',true); ?></div>

											</div>


											<div class="row intro-basic-tab">

												<span class="col-md-4">
													
													<div class="contact-head">ISBN 10</div>
												</span>
												<div class="col-md-8 contact-answ"> <?php echo get_post_meta(get_the_ID(),'wpcf-isbn-10',true); ?></div>

											</div>



											<div class="row intro-basic-tab">

												<span class="col-md-4">
													
													<div class="contact-head">ISBN 13</div>
												</span>
												<div class="col-md-8 contact-answ"> <?php echo get_post_meta(get_the_ID(),'wpcf-isbn-13',true); ?></div>

											</div>


											<div class="row intro-basic-tab">

												<span class="col-md-4">
													
													<div class="contact-head">Edition Number</div>
												</span>
												<div class="col-md-8 contact-answ"> <?php echo get_post_meta(get_the_ID(),'wpcf-edition-number',true); ?></div>

											</div>
						
								</div>

				</div>
		</div>

		<div class="course-rating">
						<h3>Reviews</h3>


						<?php echo do_shortcode('[RICH_REVIEWS_SNIPPET category="page" ]'); ?>


						<?php echo do_shortcode('[RICH_REVIEWS_SHOW num="6" category="page" ]'); ?>
						


		</div>


		<div class="review-post-form-wrapper">
					<h3 class="review-post-form-title">Post a review</h3>
					<?php echo do_shortcode('[RICH_REVIEWS_FORM category="page"]'); ?>
				</div>
				

		<div class="entry-tag-share">
			<div class="row">
				<div class="col-sm-6">
					<?php
					if ( get_the_tag_list() ) {
						//echo get_the_tag_list( '<p class="post-tag"><span>' . esc_html__( "Tag:", 'eduma' ) . '</span>', ', ', '</p>' );
					}
					?>
				</div>
				<div class="col-sm-6">
					<?php do_action( 'thim_social_share' ); ?>
				</div>
			</div>
		</div>
		<?php //do_action( 'thim_about_author' ); ?>

		<?php
		$prev_post = get_previous_post();
		$next_post = get_next_post();
		?>
		<?php if ( !empty( $prev_post ) || !empty( $next_post ) ): ?>
			<div class="entry-navigation-post">
				<?php
				if ( !empty( $prev_post ) ):
					?>
					<div class="prev-post">
						<p class="heading"><?php echo esc_html__( 'Previous Book', 'eduma' ); ?></p>
						<h5 class="title">
							<a href="<?php echo get_permalink( $prev_post->ID ); ?>"><?php echo esc_html( $prev_post->post_title ); ?></a>
						</h5>

						<div class="date">
							<?php echo get_the_date( get_option( 'date_format' ) ); ?>
						</div>
					</div>
				<?php endif; ?>

				<?php
				if ( !empty( $next_post ) ):
					?>
					<div class="next-post">
						<p class="heading"><?php echo esc_html__( 'Next Book', 'eduma' ); ?></p>
						<h5 class="title">
							<a href="<?php echo get_permalink( $next_post->ID ); ?>"><?php echo esc_html( $next_post->post_title ); ?></a>
						</h5>

						<div class="date">
							<?php echo get_the_date( 'j F, Y', $next_post->ID ); ?>
						</div>
					</div>
				<?php endif; ?>
			</div>

		<?php endif; ?>

		<?php
		get_template_part( 'inc/related' );
		?>
	</div>
</article>