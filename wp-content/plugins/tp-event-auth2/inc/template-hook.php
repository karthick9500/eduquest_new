<?php
/**
 * @Author: ducnvtt
 * @Date:   2016-03-03 09:24:04
 * @Last Modified by:   ducnvtt
 * @Last Modified time: 2016-03-10 15:45:38
 */

use TP_Event_Auth\Sessions\Sessions as Session;

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

/**
 * Template hook
 */
add_action( 'event_auth_messages', 'event_auth_display_messages' );
if ( ! function_exists( 'event_auth_display_messages' ) ) {
	function event_auth_display_messages( $atts ) {
		$sessions = Session::instance( 'event_auth_flash_session' );
		if ( $sessions->session ) {
			ob_start();
			tpe_auth_addon_get_template( 'messages.php', array( 'messages' => $sessions->session ) );
			echo ob_get_clean();
		}
		Session::instance( 'event_auth_flash_session' )->remove();
	}
}

// template hook
add_action( 'tp_event_after_loop_event_item', 'event_auth_register' );
add_action( 'tp_event_after_single_event', 'event_auth_register' );
if ( ! function_exists( 'event_auth_register' ) ) {
	function event_auth_register() {
		TP_Event_Authentication()->loader->load_module( '\TP_Event_Auth\Events\Event' )->book_event_template();
	}
}
// filter shortcode
add_filter( 'the_content', 'event_auth_content_filter' );
if ( ! function_exists( 'event_auth_content_filter' ) ) {
	function event_auth_content_filter( $content ) {
		global $post;

		if ( is_page( tpe_auth_get_page_id( 'login' ) ) ) {
			$content = '[event_auth_login]';
		} else if ( is_page( tpe_auth_get_page_id( 'register' ) ) ) {
			$content = '[event_auth_register]';
		} else if ( is_page( tpe_auth_get_page_id( 'forgot_pass' ) ) ) {
			$content = '[event_auth_forgot_password]';
		} else if ( is_page( tpe_auth_get_page_id( 'reset_password' ) ) ) {
			$content = '[event_auth_reset_password]';
		} else if ( is_page( tpe_auth_get_page_id( 'account' ) ) ) {
			$content = '[event_auth_my_account]';
		}

		return $content;
	}
}
